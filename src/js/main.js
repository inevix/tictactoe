const game = document.querySelector('#app');
const scoreBox = document.querySelector('#score');
const resetBtn = document.querySelector('#reset');
const tiles = 9;
const x = 'x';
const o = 'o';
const squareClass = 'js-square';
const squareClassSelector = `.${squareClass}`;
const classDisable = 'is-disable';
const classActive = 'is-active';
const firstStep = x;
const defaultScore = {
    x: 0,
    o: 0
};

let step = firstStep,
    checkedTiles = 0,
    result = {
        finished: false,
        winner: false
    };

const newGame = () => {
    resetGame();
    for (let i = 0; i < tiles; i++) game.innerHTML += `<div class="${squareClass} square"/>`;
    updateScore();
    squaresClick();
};

const resetClick = () => {
    resetBtn.addEventListener('click', function() {
        setScore(defaultScore);
        newGame();
    });
};

const resetGame = () => {
    game.innerHTML = '';
    game.classList.remove(classDisable);
    checkedTiles = 0;
    result.finished = false;
    result.winner = false;
    step = firstStep;
};

const squaresClick = () => {
    let squares = document.querySelectorAll(squareClassSelector);

    squares.forEach(square => {
        square.addEventListener('click', function() {
            if (!this.innerHTML.length && !game.classList.contains(classDisable)) {
                checkedTiles++;
                this.innerHTML = step;
                this.classList.add(`square--${step}`);
                step = step === x ? o : x;
                window.getSelection().removeAllRanges();
                updateApp();
                if (checkedTiles === tiles) {
                    game.classList.add(classDisable);
                    result.finished = true;
                }
                if (result.finished && !result.winner) setTimeout(newGame, 2000);
            }
        });
    });
};

const updateApp = () => {
    checkSquares(0, 1, 2);
    checkSquares(3, 4, 5);
    checkSquares(6, 7, 8);
    checkSquares(0, 3, 6);
    checkSquares(1, 4, 7);
    checkSquares(2, 5, 8);
    checkSquares(0, 4, 8);
    checkSquares(2, 4, 6);
};

const checkSquares = (a, b, c) => {
    let players = [x, o];

    players.forEach(player => {
        if (checkSquare(a, player) && checkSquare(b, player) && checkSquare(c, player)) {
            result.finished = true;
            result.winner = player;
            game.classList.add(classDisable);
            highlight(a, b, c);
            updateScore(result.winner);
            setTimeout(newGame, 2000);
        }
    });
};

const checkSquare = (value, player) => {
    return document.querySelectorAll(squareClassSelector)[value].innerHTML === player;
};

const highlight = (...values) => {
    let squares = document.querySelectorAll(squareClassSelector);

    squares.forEach((square, index) => {
        if (values.includes(index)) return square.classList.add(classActive);
    });
};

const updateScore = player => {
    let score = getScore();

    if (player !== undefined) {
        player === x ? score.x++ : score.o++;
        setScore(score);
    }

    let scoreBoxX = `<span><span class="x">x</span>: ${score.x}</span>`,
        scoreBoxO = `<span><span class="o">o</span>: ${score.o}</span>`;

    scoreBox.innerHTML = `${scoreBoxX + scoreBoxO}`;
};

const setScore = score => {
    window.localStorage.setItem('score', JSON.stringify(score));
};

const getScore = () => {
    let score = JSON.parse(window.localStorage.getItem('score'));

    return score !== null ? score : defaultScore;
};

document.addEventListener('DOMContentLoaded', newGame);
document.addEventListener('DOMContentLoaded', resetClick);