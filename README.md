## Tic Tac Toe
https://tictactoe.devplace.de/  

### CONTACTS
https://inevix.biz/  
ineviix@gmail.com

### GENERAL INFO:
1. The toolkit: **Gulp 4**.
2. The preprocessors: **SASS**, **BABEL**.
3. Frameworks: **none**.
4. The JS library: **jQuery**.
5. Support: **ES6**.

### HOW TO USE IT:
1. Open a terminal and input: `npm install --save-dev`.
2. Run the task: `gulp`.
3. Make all changes in files of the folder: **src/**.

#### - HTML:
- Edit a home page in the file: **src/index.html**.
- Use the **gulp-rigger** for the html files.
- The layout and the connections are in the files: **src/layouts/**.

### - SCSS:
- Make all changes in the files: **src/scss/**.

### - JS:
- Make the general changes in the file: **src/js/main.js**.