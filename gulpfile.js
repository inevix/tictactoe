'use strict';

let configServer = {
        notify: false,
        server: {
            baseDir: "./"
        }
    },
    path = {
        build: {
            html: './',
            js: 'js/',
            css: 'css/'
        },
        src: {
            html: 'src/*.html',
            js: 'src/js/main.js',
            style: 'src/scss/style.scss'
        },
        watch: {
            html: 'src/**/*.html',
            js: 'src/js/**/*.js',
            css: 'src/scss/**/*.scss'
        },
        clean: 'js/, css/'
    },
    gulp = require('gulp'),
    browsersync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    rigger = require('gulp-rigger'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    del = require('del'),
    babel = require('gulp-babel');

function browserSync(done) {
    browsersync.init(configServer);
    done();
}

function browserSyncReload(done) {
    browsersync.reload();
    done();
}

function buildHtml() {
    return gulp
        .src(path.src.html)
        .pipe(rigger())
        .pipe(plumber())
        .pipe(gulp.dest(path.build.html))
        .pipe(browsersync.stream());
}

function buildCss() {
    return gulp
        .src(path.src.style)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.build.css))
        .pipe(browsersync.stream());
}

function buildJs() {
    return gulp
        .src(path.src.js)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/env', 'minify'],
            comments: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(path.build.js))
        .pipe(browsersync.stream());
}

function buildClean() {
    return del([path.clean]);
}

function watchFiles() {
    gulp.watch(path.watch.html, gulp.series(buildHtml, browserSyncReload));
    gulp.watch(path.watch.css, gulp.series(buildCss, browserSyncReload));
    gulp.watch(path.watch.js, gulp.series(buildJs, browserSyncReload));
}

exports.build = gulp.series(buildClean, gulp.parallel(buildHtml, buildCss, buildJs));
exports.default = gulp.parallel(watchFiles, browserSync);